package nl.phazebroek.boostyourvelocity.greeting;

import static org.assertj.core.api.BDDAssertions.then;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class GreetingRepositoryTest {

  @InjectMocks
  private GreetingRepository greetingRepository;

  @ParameterizedTest
  @CsvSource({"Koen", "Pim", "Rockstar"})
  void findGreetingById(String id) {
    var greeting = greetingRepository.findGreetingById(id);

    then(greeting).isEqualTo("Hello, " + id + "!");
  }

  @Test
  void findDefaultGreeting() {
    var greeting = greetingRepository.findGreetingById("foo");

    then(greeting).isEqualTo("Hello, World!");
  }

}