package nl.phazebroek.boostyourvelocity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main entry point for the app.
 */
@SpringBootApplication
public class BoostYourVelocityApplication {

  public static void main(String[] args) {
    SpringApplication.run(BoostYourVelocityApplication.class, args);
  }

}
